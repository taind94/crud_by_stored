﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProductForm
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.dgvUser = New System.Windows.Forms.DataGridView()
    Me.txtxName = New System.Windows.Forms.TextBox()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.btnAdd = New System.Windows.Forms.Button()
    Me.btnEdit = New System.Windows.Forms.Button()
    Me.btnDelete = New System.Windows.Forms.Button()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.txtID = New System.Windows.Forms.TextBox()
    CType(Me.dgvUser, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'dgvUser
    '
    Me.dgvUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.dgvUser.Location = New System.Drawing.Point(27, 177)
    Me.dgvUser.MultiSelect = False
    Me.dgvUser.Name = "dgvUser"
    Me.dgvUser.ReadOnly = True
    Me.dgvUser.Size = New System.Drawing.Size(389, 287)
    Me.dgvUser.TabIndex = 0
    '
    'txtxName
    '
    Me.txtxName.Location = New System.Drawing.Point(270, 28)
    Me.txtxName.Name = "txtxName"
    Me.txtxName.Size = New System.Drawing.Size(100, 20)
    Me.txtxName.TabIndex = 2
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(197, 31)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(38, 13)
    Me.Label1.TabIndex = 3
    Me.Label1.Text = " Name"
    '
    'btnAdd
    '
    Me.btnAdd.Location = New System.Drawing.Point(64, 86)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(75, 23)
    Me.btnAdd.TabIndex = 5
    Me.btnAdd.Text = "New"
    Me.btnAdd.UseVisualStyleBackColor = True
    '
    'btnEdit
    '
    Me.btnEdit.Location = New System.Drawing.Point(160, 86)
    Me.btnEdit.Name = "btnEdit"
    Me.btnEdit.Size = New System.Drawing.Size(75, 23)
    Me.btnEdit.TabIndex = 6
    Me.btnEdit.Text = "Edit"
    Me.btnEdit.UseVisualStyleBackColor = True
    '
    'btnDelete
    '
    Me.btnDelete.Location = New System.Drawing.Point(258, 86)
    Me.btnDelete.Name = "btnDelete"
    Me.btnDelete.Size = New System.Drawing.Size(75, 23)
    Me.btnDelete.TabIndex = 7
    Me.btnDelete.Text = "Remove"
    Me.btnDelete.UseVisualStyleBackColor = True
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(61, 31)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(18, 13)
    Me.Label2.TabIndex = 9
    Me.Label2.Text = "ID"
    '
    'txtID
    '
    Me.txtID.Location = New System.Drawing.Point(91, 28)
    Me.txtID.Name = "txtID"
    Me.txtID.ReadOnly = True
    Me.txtID.Size = New System.Drawing.Size(68, 20)
    Me.txtID.TabIndex = 1
    '
    'ProductForm
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(446, 476)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.txtID)
    Me.Controls.Add(Me.btnDelete)
    Me.Controls.Add(Me.btnEdit)
    Me.Controls.Add(Me.btnAdd)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.txtxName)
    Me.Controls.Add(Me.dgvUser)
    Me.Name = "ProductForm"
    Me.Text = "Product"
    CType(Me.dgvUser, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

  Friend WithEvents dgvUser As DataGridView
  Friend WithEvents txtxName As TextBox
  Friend WithEvents Label1 As Label
  Friend WithEvents btnAdd As Button
  Friend WithEvents btnEdit As Button
  Friend WithEvents btnDelete As Button
  Friend WithEvents Label2 As Label
  Friend WithEvents txtID As TextBox
End Class
