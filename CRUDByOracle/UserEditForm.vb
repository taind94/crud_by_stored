﻿Public Class UserEditForm
#Region "Declare"
  Private _cn As DbConnection = New DbConnection()
  Private _isChanged As Boolean = False
#End Region

#Region "Init"
  Public Sub New(intUserID As Integer)

    ' This call is required by the designer.
    InitializeComponent()

    Try
      Dim strQuery As String
      Dim param As DbParameter = New DbParameter()
      Dim dt As DataTable = New DataTable

      'Fill data for Status combobox
      cbbStatus.DataSource = [Enum].GetValues(GetType(CommonConstant.Status))
      'Fill data for Department combobox
      LoadDepartmentCombobox()

      'Get User by ID 
      txtUserID.Text = intUserID
      strQuery = "CRUD_USERS.GetUserByID"
      param.AddParemeter("p_ID", DbParameter.DbType.INTEGER_, 4, txtUserID.Text)
      param.AddParemeter("cur_Users", DbParameter.DbType.Cursor, Nothing, Nothing, ParameterDirection.Output)

      _cn.Open()
      _cn.FillStored(strQuery, param, dt)
      _cn.Close()

      If dt.Rows.Count > 0 Then
        txtUserName.Text = dt.Rows(0).Field(Of String)("USERNAME")
        txtEmail.Text = dt.Rows(0).Field(Of String)("EMAIL")
        cbbDepartment.SelectedValue = dt.Rows(0)("DEPARTMENT_CODE")
        cbbStatus.SelectedIndex = dt.Rows(0).Field(Of Decimal)("STATUS")
      End If
    Catch ex As Exception
      MsgBox(ex.Message)
    End Try
  End Sub
#End Region

#Region "Method"
  ''' <summary>
  ''' Clears the screen.
  ''' </summary>
  Private Sub ClearScreen()
    Try
      'Clear screen
      txtUserName.Text = String.Empty
      txtEmail.Text = String.Empty
      cbbStatus.DataSource = [Enum].GetValues(GetType(CommonConstant.Status))
      cbbStatus.SelectedIndex = CommonConstant.Status.Active

      LoadDepartmentCombobox()
    Catch ex As Exception
      MsgBox(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' Loads the department combobox.
  ''' </summary>
  Private Sub LoadDepartmentCombobox()
    Try
      Dim strQuery As String
      Dim param As DbParameter = New DbParameter()
      Dim dt As DataTable = New DataTable

      strQuery = "CRUD_DEPARTMENTS.GetDepartmentList"
      param.AddParemeter("cur_Departments", DbParameter.DbType.Cursor, Nothing, Nothing, ParameterDirection.Output)

      _cn.Open()
      _cn.FillStored(strQuery, param, dt)
      cbbDepartment.DataSource = dt
      cbbDepartment.DisplayMember = "DEPARTMENT_NAME"
      cbbDepartment.ValueMember = "DEPARTMENT_CODE"
      _cn.Close()
    Catch ex As Exception
      MsgBox(ex.Message)
    End Try
  End Sub
#End Region

#Region "Event"
  Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
    Try
      Dim strQuery As String
      Dim param As DbParameter = New DbParameter()
      Dim intResult As Integer = 0

      'Validate fields
      If Len(Trim(txtUserName.Text)) <= 8 OrElse Len(Trim(txtUserName.Text)) > 20 Then
        MsgBox("Username is invalid.")
        Return
      ElseIf Len(Trim(txtEmail.Text)) <= 0 OrElse Len(Trim(txtEmail.Text)) > 50 Then
        MsgBox("Email is invalid.")
        Return
      End If

      'Edit User
      strQuery = "CRUD_USERS.EditUser"
      param.AddParemeter("p_ID", DbParameter.DbType.INTEGER_, 4, txtUserID.Text)
      param.AddParemeter("p_Email", DbParameter.DbType.NVARCHAR, 50, Trim(txtEmail.Text))
      param.AddParemeter("p_DepartmentID", DbParameter.DbType.NVARCHAR, 10, cbbDepartment.SelectedValue)
      param.AddParemeter("p_Status", DbParameter.DbType.INTEGER_, 4, cbbStatus.SelectedValue)
      param.AddParemeter("p_UserName", DbParameter.DbType.NVARCHAR, 20, Trim(txtUserName.Text))
      param.AddParemeter("out_result", DbParameter.DbType.INTEGER_, 4, intResult, ParameterDirection.Output)

      _cn.Open()
      intResult = _cn.ExecuteStored(strQuery, param)
      _cn.Close()
      If intResult = CommonConstant.SQLResult.Successful Then
        _isChanged = False
        MsgBox("Successfully")
      Else
        MsgBox("Wrong")
      End If
    Catch ex As Exception
      _cn.Close()
      MsgBox(ex.Message)
    End Try
  End Sub

  Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
    ClearScreen()
  End Sub

  Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
    Try
      If _isChanged Then
        Dim dlg As DialogResult = MessageBox.Show("Are you want to exit?", "Warning!", MessageBoxButtons.YesNo)
        If dlg = DialogResult.Yes Then
          Me.Close()
        End If
      Else
        Me.Close()
      End If
    Catch ex As Exception
      MsgBox(ex.Message)
    End Try
  End Sub

  Private Sub cbbDepartment_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbDepartment.SelectedIndexChanged, cbbStatus.SelectedIndexChanged
    _isChanged = True
  End Sub

  Private Sub txtEmail_TextChanged(sender As Object, e As EventArgs) Handles txtEmail.TextChanged, txtUserName.TextChanged
    _isChanged = True
  End Sub

  Private Sub UserEditForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    _isChanged = False
  End Sub
#End Region
End Class