﻿Public Class UserCreateForm

#Region "Declare"
  Private _cn As DbConnection = New DbConnection()
  Private _isChanged As Boolean = False
#End Region

#Region "Method"
  ''' <summary>
  ''' Clears the screen.
  ''' </summary>
  Private Sub ClearScreen()
    Try
      txtUserName.Text = String.Empty
      txtEmail.Text = String.Empty
      txtPassword.Text = String.Empty
      txtConfirm.Text = String.Empty
      cbbStatus.DataSource = [Enum].GetValues(GetType(CommonConstant.Status))
      cbbStatus.SelectedIndex = CommonConstant.Status.Active

      Dim strQuery As String
      Dim param As DbParameter = New DbParameter()
      Dim dt As DataTable = New DataTable

      strQuery = "CRUD_DEPARTMENTS.GetDepartmentList"
      param.AddParemeter("cur_Departments", DbParameter.DbType.Cursor, Nothing, Nothing, ParameterDirection.Output)

      _cn.Open()
      _cn.FillStored(strQuery, param, dt)
      cbbDepartment.DataSource = dt
      cbbDepartment.DisplayMember = "DEPARTMENT_NAME"
      cbbDepartment.ValueMember = "DEPARTMENT_CODE"
      _cn.Close()
    Catch ex As Exception
      MsgBox(ex.Message)
    End Try
  End Sub
#End Region

#Region "Event"
  Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
    Try
      If _isChanged Then
        Dim dlg As DialogResult = MessageBox.Show("Are you want to exit?", "Warning!", MessageBoxButtons.YesNo)
        If dlg = DialogResult.Yes Then
          Me.Close()
        End If
      Else
        Me.Close()
      End If
    Catch ex As Exception
      MsgBox(ex.Message)
    End Try
  End Sub

  Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
    ClearScreen()
  End Sub

  Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
    Try
      Dim strQuery As String
      Dim param As DbParameter = New DbParameter()
      Dim intResult As Integer = 0

      'Validate form
      If Len(Trim(txtUserName.Text)) <= 8 OrElse Len(Trim(txtUserName.Text)) > 20 Then
        MsgBox("Username is invalid.")
        Return
      ElseIf Len(Trim(txtEmail.Text)) <= 0 OrElse Len(Trim(txtEmail.Text)) > 50 Then
        MsgBox("Email is invalid.")
        Return
      ElseIf Len(Trim(txtPassword.Text)) <= 8 OrElse Len(Trim(txtPassword.Text)) > 20 Then
        MsgBox("Password is invalid.")
        Return
      ElseIf String.Compare(Trim(txtPassword.Text), Trim(txtConfirm.Text)) <> 0 Then
        MsgBox("Password confirm is not match.")
        Return
      End If

      strQuery = "CRUD_USERS.CreateUser"

      param.AddParemeter("p_Email", DbParameter.DbType.NVARCHAR, 50, Trim(txtEmail.Text))
      param.AddParemeter("P_DepartmentID", DbParameter.DbType.NVARCHAR, 100, cbbDepartment.SelectedValue)
      param.AddParemeter("p_Status", DbParameter.DbType.NVARCHAR, 1, cbbStatus.SelectedIndex)
      param.AddParemeter("p_Password", DbParameter.DbType.NVARCHAR, 100, CommonHelper.Hash(CommonConstant.SALT, Trim(txtPassword.Text)))
      param.AddParemeter("p_UserName", DbParameter.DbType.NVARCHAR, 20, Trim(txtUserName.Text))
      param.AddParemeter("out_result", DbParameter.DbType.INTEGER_, 1, intResult, ParameterDirection.Output)

      _cn.Open()
      intResult = _cn.ExecuteStored(strQuery, param)
      _cn.Close()
      If intResult = CommonConstant.SQLResult.Successful Then
        _isChanged = False
        MsgBox("Successfully")
      Else
        MsgBox("Wrong")
      End If
    Catch ex As Exception
      _cn.Close()
      MsgBox(ex.Message)
    End Try
  End Sub

  Private Sub UserCreateForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    Try
      ClearScreen()
      _isChanged = False
    Catch ex As Exception
      MsgBox(ex.Message)
    End Try
  End Sub

  Private Sub textboxChange_TextChanged(sender As Object, e As EventArgs) Handles txtUserName.TextChanged, txtConfirm.TextChanged, txtEmail.TextChanged, txtPassword.TextChanged
    _isChanged = True
  End Sub

  Private Sub cbbStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbStatus.SelectedIndexChanged, cbbDepartment.SelectedIndexChanged
    _isChanged = True
  End Sub
#End Region
End Class