﻿Imports System.IO

Public Class UserListForm
#Region "Declare"
  Private _cn As DbConnection = New DbConnection()
  Private _dt As DataTable = New DataTable()
  Private _checkBoxCheckAll As CheckBox

  Enum UserColumnName
    CheckBox = 0
    ID = 1
    UserName = 2
    Email = 3
    Department = 4
    Status = 5
    CreatedAt = 6
  End Enum
#End Region

#Region "Init"
  ''' <summary>
  ''' Initializations this instance.
  ''' </summary>
  Private Sub Initialization()
    Try
      txtSearch.Text = CommonConstant.SEARCH_PLACEHOLDER
      lblDateTime.Text = DateTime.Now.ToString(CommonConstant.DATE_FORMAT)
    Catch ex As Exception
      MsgBox(ex.Message)
    End Try
  End Sub
#End Region

#Region "Method"
  ''' <summary>
  ''' Loads the gridview.
  ''' </summary>
  Private Sub LoadGridview()
    Try
      Me.ControlBox = False
      Me.Visible = True

      _dt = New DataTable()
      Dim param As DbParameter = New DbParameter()
      Dim strQuery As String
      Dim strSearchText As String = IIf(Trim(txtSearch.Text) = CommonConstant.SEARCH_PLACEHOLDER, vbNullString, Trim(txtSearch.Text))
      strQuery = "CRUD_USERS.GetUserList"

      param.AddParemeter("p_Search", DbParameter.DbType.NVARCHAR, 20, IIf(strSearchText <> vbNullString, strSearchText, DBNull.Value))
      param.AddParemeter("cur_Users", DbParameter.DbType.Cursor, Nothing, Nothing, ParameterDirection.Output)

      _cn.Open()
      _cn.FillStored(strQuery, param, _dt)
      _cn.Close()
      dgvUser.Columns.Clear()
      Dim colCheckbox As DataGridViewCheckBoxColumn = New DataGridViewCheckBoxColumn()
      colCheckbox.Name = String.Empty
      colCheckbox.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
      dgvUser.Columns.Add(colCheckbox)
      _checkBoxCheckAll = New CheckBox()
      Dim rect As Rectangle = dgvUser.GetCellDisplayRectangle(0, -1, True)
      _checkBoxCheckAll.Size = New Size(18, 18)

      'Change the location of the CheckBox to make it stay on the header
      _checkBoxCheckAll.Location = New Point(rect.Location.X + 3, rect.Location.Y + 2)

      'Add event
      AddHandler _checkBoxCheckAll.CheckedChanged, AddressOf ckBox_CheckedChanged

      With dgvUser
        'Add the CheckBox into the DataGridView
        .Controls.Add(_checkBoxCheckAll)

        'Fill data
        .DataSource = _dt

        'Format grid view
        .Columns(UserColumnName.UserName).HeaderText = "User name"
        .Columns(UserColumnName.Email).HeaderText = "Email"
        .Columns(UserColumnName.Department).HeaderText = "Department"
        .Columns(UserColumnName.Status).HeaderText = "Status"
        .Columns(UserColumnName.CreatedAt).HeaderText = "CreateAt"
        .Columns(UserColumnName.ID).Visible = False
        .Columns(UserColumnName.CheckBox).ReadOnly = False
        .Columns(UserColumnName.CheckBox).Width = 20
        .Columns(UserColumnName.UserName).ReadOnly = True
        .Columns(UserColumnName.Email).ReadOnly = True
        .Columns(UserColumnName.Department).ReadOnly = True
        .Columns(UserColumnName.Status).ReadOnly = True
        .Columns(UserColumnName.CreatedAt).ReadOnly = True
        .AutoResizeColumns(autoSizeColumnsMode:=DataGridViewAutoSizeColumnsMode.AllCells)

        'Clear content
        .ClearSelection()
      End With

    Catch ex As Exception
      MsgBox(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' Gets the checked array identifier.
  ''' </summary>
  ''' <returns>System.String.</returns>
  Private Function GetCheckedArrayID() As String
    Try
      Dim sb = New Text.StringBuilder
      Dim isAddComma As Boolean = False
      If Me.dgvUser.RowCount <= 0 Then Return String.Empty

      For intCount As Integer = 0 To Me.dgvUser.RowCount - 1
        If Me.dgvUser(UserColumnName.CheckBox, intCount).Value Then
          If isAddComma Then
            sb.Append(",")
          End If

          sb.Append(Me.dgvUser(UserColumnName.ID, intCount).Value)
          isAddComma = True
        End If
      Next

      Return sb.ToString
    Catch ex As Exception
      MsgBox(ex.Message)
      Return String.Empty
    End Try
  End Function

  ''' <summary>
  ''' Exports the CSV file.
  ''' </summary>
  Private Sub ExportCSVFile()
    'Build the CSV file data as a Comma separated string.
    Dim sb = New Text.StringBuilder
    Dim cellValue As Object

    'Add the Header row for CSV file.
    For Each column As DataGridViewColumn In dgvUser.Columns
      If column.Index <> 0 Then
        sb.Append(column.HeaderText)
        sb.Append(","c)
      End If
    Next

    'Add new line.
    sb.Append(vbCr)
    sb.Append(vbLf)
    'Adding the Rows
    For Each row As DataGridViewRow In dgvUser.Rows
      For Each cell As DataGridViewCell In row.Cells
        If cell.Value Is Nothing Then
          cellValue = String.Empty
        Else
          cellValue = cell.Value
        End If

        If cell.ColumnIndex <> 0 Then
          'Add the Data rows.
          sb.Append(cellValue.ToString().Replace(",", ";"))
          sb.Append(","c)
        End If
      Next

      'Add new line.
      sb.Append(vbCr)
      sb.Append(vbLf)
    Next

    'Exporting to Excel
    Dim strFolderPath As String = "D:\"
    File.WriteAllText(strFolderPath & "DataGridViewExport.csv", sb.ToString)
    MsgBox("Export file successfully!")
  End Sub
#End Region

#Region "Event"
  Public Sub ckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
    For intIndex As Integer = 0 To Me.dgvUser.RowCount - 1
      Me.dgvUser(UserColumnName.CheckBox, intIndex).Value = _checkBoxCheckAll.Checked
    Next

    Me.dgvUser.EndEdit()
  End Sub

  Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
    Try
      'Clear textbox
      txtSearch.Text = CommonConstant.SEARCH_PLACEHOLDER

      Dim frmUserCreate As UserCreateForm = New UserCreateForm()
      frmUserCreate.ShowDialog()
      LoadGridview()
    Catch ex As Exception
      MsgBox(ex.Message)
    End Try
  End Sub

  Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
    Try
      'Clear textbox
      txtSearch.Text = CommonConstant.SEARCH_PLACEHOLDER

      If Len(dgvUser.CurrentRow.Cells(UserColumnName.ID).Value) > 0 Then
        Dim frmUserEdit As UserEditForm = New UserEditForm(dgvUser.CurrentRow.Cells(UserColumnName.ID).Value)
        frmUserEdit.ShowDialog()
        LoadGridview()
      Else
        MsgBox("Choose a row", "Warning!")
      End If

    Catch ex As Exception
      MsgBox(ex.Message)
    End Try
  End Sub

  Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
    Try
      Dim strQuery As String
      Dim intResult As Integer = 0
      Dim strArrID As String
      Dim param As DbParameter = New DbParameter()
      strArrID = GetCheckedArrayID()
      If String.IsNullOrEmpty(strArrID) Then
        MsgBox("Check a row")
        Return
      End If
      strQuery = "CRUD_USERS.RemoveUsers"
      param.AddParemeter("arr_ID", DbParameter.DbType.NVARCHAR, 1000, strArrID)
      param.AddParemeter("out_result", DbParameter.DbType.INTEGER_, 1, intResult, ParameterDirection.Output)

      _cn.Open()
      intResult = _cn.ExecuteStored(strQuery, param)
      _cn.Close()

      If intResult = CommonConstant.SQLResult.Successful Then
        MsgBox("Successfully")
      Else
        MsgBox("Wrong", "Warning!")
      End If
      'Reload grid view
      LoadGridview()

    Catch ex As Exception
      _cn.Close()
      MsgBox(ex.Message)
    End Try
  End Sub

  Private Sub UserListForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    Try
      'Initialization form
      Initialization()
      'Load data in grid view
      LoadGridview()
      TimerDate.Start()
    Catch ex As Exception
      MsgBox(ex.Message)
    End Try
  End Sub

  Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
    LoadGridview()
  End Sub

  Private Sub txtSearch_MouseClick(sender As Object, e As MouseEventArgs) Handles txtSearch.MouseClick
    If Trim(txtSearch.Text) = CommonConstant.SEARCH_PLACEHOLDER Then
      txtSearch.Text = String.Empty
    End If
  End Sub

  Private Sub txtSearch_Leave(sender As Object, e As EventArgs) Handles txtSearch.Leave
    If Trim(txtSearch.Text) = vbNullString Then
      txtSearch.Text = CommonConstant.SEARCH_PLACEHOLDER
    End If
  End Sub

  Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
    Me.Close()
  End Sub

  Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
    Try
      ExportCSVFile()
    Catch ex As Exception
      MsgBox(ex.Message)
    End Try

  End Sub

  Private Sub TimerDate_Tick(sender As Object, e As EventArgs) Handles TimerDate.Tick
    lblDateTime.Text = DateTime.Now.ToString(CommonConstant.DATE_FORMAT)
  End Sub
#End Region

End Class