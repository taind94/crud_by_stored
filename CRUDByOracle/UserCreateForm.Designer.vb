﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class UserCreateForm
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()>
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()>
  Private Sub InitializeComponent()
    Me.btnBack = New System.Windows.Forms.Button()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.btnCreate = New System.Windows.Forms.Button()
    Me.btnClear = New System.Windows.Forms.Button()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.lblUsername = New System.Windows.Forms.Label()
    Me.txtEmail = New System.Windows.Forms.TextBox()
    Me.txtUserName = New System.Windows.Forms.TextBox()
    Me.Label5 = New System.Windows.Forms.Label()
    Me.txtPassword = New System.Windows.Forms.TextBox()
    Me.cbbDepartment = New System.Windows.Forms.ComboBox()
    Me.cbbStatus = New System.Windows.Forms.ComboBox()
    Me.Label6 = New System.Windows.Forms.Label()
    Me.txtConfirm = New System.Windows.Forms.TextBox()
    Me.Panel3 = New System.Windows.Forms.Panel()
    Me.Label4 = New System.Windows.Forms.Label()
    Me.Panel3.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnBack
    '
    Me.btnBack.Location = New System.Drawing.Point(29, 369)
    Me.btnBack.Name = "btnBack"
    Me.btnBack.Size = New System.Drawing.Size(75, 23)
    Me.btnBack.TabIndex = 17
    Me.btnBack.Text = "Back"
    Me.btnBack.UseVisualStyleBackColor = True
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(36, 173)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(37, 13)
    Me.Label2.TabIndex = 16
    Me.Label2.Text = "Status"
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(36, 134)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(62, 13)
    Me.Label1.TabIndex = 15
    Me.Label1.Text = "Department"
    '
    'btnCreate
    '
    Me.btnCreate.Location = New System.Drawing.Point(353, 369)
    Me.btnCreate.Name = "btnCreate"
    Me.btnCreate.Size = New System.Drawing.Size(75, 23)
    Me.btnCreate.TabIndex = 13
    Me.btnCreate.Text = "Create new"
    Me.btnCreate.UseVisualStyleBackColor = True
    '
    'btnClear
    '
    Me.btnClear.Location = New System.Drawing.Point(250, 369)
    Me.btnClear.Name = "btnClear"
    Me.btnClear.Size = New System.Drawing.Size(75, 23)
    Me.btnClear.TabIndex = 12
    Me.btnClear.Text = "Clear"
    Me.btnClear.UseVisualStyleBackColor = True
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(36, 99)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(32, 13)
    Me.Label3.TabIndex = 21
    Me.Label3.Text = "Email"
    '
    'lblUsername
    '
    Me.lblUsername.AutoSize = True
    Me.lblUsername.Location = New System.Drawing.Point(36, 67)
    Me.lblUsername.Name = "lblUsername"
    Me.lblUsername.Size = New System.Drawing.Size(55, 13)
    Me.lblUsername.TabIndex = 20
    Me.lblUsername.Text = "Username"
    '
    'txtEmail
    '
    Me.txtEmail.Location = New System.Drawing.Point(138, 92)
    Me.txtEmail.Name = "txtEmail"
    Me.txtEmail.Size = New System.Drawing.Size(169, 20)
    Me.txtEmail.TabIndex = 19
    '
    'txtUserName
    '
    Me.txtUserName.Location = New System.Drawing.Point(138, 60)
    Me.txtUserName.Name = "txtUserName"
    Me.txtUserName.Size = New System.Drawing.Size(169, 20)
    Me.txtUserName.TabIndex = 18
    '
    'Label5
    '
    Me.Label5.AutoSize = True
    Me.Label5.Location = New System.Drawing.Point(36, 210)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(53, 13)
    Me.Label5.TabIndex = 23
    Me.Label5.Text = "Password"
    '
    'txtPassword
    '
    Me.txtPassword.Location = New System.Drawing.Point(138, 203)
    Me.txtPassword.Name = "txtPassword"
    Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
    Me.txtPassword.Size = New System.Drawing.Size(169, 20)
    Me.txtPassword.TabIndex = 22
    '
    'cbbDepartment
    '
    Me.cbbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cbbDepartment.FormattingEnabled = True
    Me.cbbDepartment.Location = New System.Drawing.Point(138, 126)
    Me.cbbDepartment.Name = "cbbDepartment"
    Me.cbbDepartment.Size = New System.Drawing.Size(169, 21)
    Me.cbbDepartment.TabIndex = 24
    '
    'cbbStatus
    '
    Me.cbbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cbbStatus.FormattingEnabled = True
    Me.cbbStatus.Location = New System.Drawing.Point(138, 165)
    Me.cbbStatus.Name = "cbbStatus"
    Me.cbbStatus.Size = New System.Drawing.Size(169, 21)
    Me.cbbStatus.TabIndex = 25
    '
    'Label6
    '
    Me.Label6.AutoSize = True
    Me.Label6.Location = New System.Drawing.Point(36, 246)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(93, 13)
    Me.Label6.TabIndex = 27
    Me.Label6.Text = "Comfirm Password"
    '
    'txtConfirm
    '
    Me.txtConfirm.Location = New System.Drawing.Point(138, 239)
    Me.txtConfirm.Name = "txtConfirm"
    Me.txtConfirm.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
    Me.txtConfirm.Size = New System.Drawing.Size(169, 20)
    Me.txtConfirm.TabIndex = 26
    '
    'Panel3
    '
    Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
    Me.Panel3.Controls.Add(Me.Label4)
    Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
    Me.Panel3.Location = New System.Drawing.Point(0, 0)
    Me.Panel3.Name = "Panel3"
    Me.Panel3.Size = New System.Drawing.Size(458, 44)
    Me.Panel3.TabIndex = 28
    '
    'Label4
    '
    Me.Label4.AutoSize = True
    Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label4.Location = New System.Drawing.Point(12, 15)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(81, 15)
    Me.Label4.TabIndex = 0
    Me.Label4.Text = "Create user"
    '
    'UserCreateForm
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(458, 410)
    Me.Controls.Add(Me.Panel3)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.txtConfirm)
    Me.Controls.Add(Me.cbbStatus)
    Me.Controls.Add(Me.cbbDepartment)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.txtPassword)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.lblUsername)
    Me.Controls.Add(Me.txtEmail)
    Me.Controls.Add(Me.txtUserName)
    Me.Controls.Add(Me.btnBack)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.btnCreate)
    Me.Controls.Add(Me.btnClear)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
    Me.Name = "UserCreateForm"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "Create new"
    Me.Panel3.ResumeLayout(False)
    Me.Panel3.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

  Friend WithEvents btnBack As Button
  Friend WithEvents Label2 As Label
  Friend WithEvents Label1 As Label
  Friend WithEvents btnCreate As Button
  Friend WithEvents btnClear As Button
  Friend WithEvents Label3 As Label
  Friend WithEvents lblUsername As Label
  Friend WithEvents txtEmail As TextBox
  Friend WithEvents txtUserName As TextBox
  Friend WithEvents Label5 As Label
  Friend WithEvents txtPassword As TextBox
  Friend WithEvents cbbDepartment As ComboBox
  Friend WithEvents cbbStatus As ComboBox
  Friend WithEvents Label6 As Label
  Friend WithEvents txtConfirm As TextBox
  Friend WithEvents Panel3 As Panel
  Friend WithEvents Label4 As Label
End Class
