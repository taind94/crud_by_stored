﻿Public Class CommonConstant
  Public Enum Status
    NotActive = 0
    Active = 1
  End Enum

  Public Enum SQLResult
    NotSuccessful = 0
    Successful = 1
  End Enum

  Public Shared ReadOnly Property SEARCH_PLACEHOLDER As String = "Search by user name or email...."
  Public Shared ReadOnly Property DATE_FORMAT As String = "yyyy/MM/dd HH:mm"

  Public Shared ReadOnly Property SALT As String = ":]q>N;B5%JI+Kjl3>r;SkeX/]ow&g%Y`7)PePjWOE+7[?=]\z2@87e&<1Ru^IIl("
End Class
