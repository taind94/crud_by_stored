﻿Imports Microsoft.VisualBasic.CompilerServices
Imports System.Data.OracleClient
Imports System.IO
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Text

Public Class DbConnection
  Implements IDisposable

  Protected mConnectionString As String
  Private mOraConnection As OracleConnection
  Private mOraTran As OracleTransaction
  Private mOraCommand As OracleCommand
  Private mOraDataAdapter As OracleDataAdapter
  Private disposedValue As Boolean

  Public Sub New()
    Me.mConnectionString = CStr(Nothing)
    Me.mOraConnection = CType(Nothing, OracleConnection)
    Me.mOraTran = CType(Nothing, OracleTransaction)
    Me.mOraCommand = CType(Nothing, OracleCommand)
    Me.mOraDataAdapter = CType(Nothing, OracleDataAdapter)
    Me.disposedValue = False
    Me.mConnectionString = GetDBConnection()
  End Sub

  Private ReadOnly Property OraConnection As OracleConnection
    Get
      Return Me.mOraConnection
    End Get
  End Property

  Private ReadOnly Property OraTransaction As OracleTransaction
    Get
      Return Me.mOraTran
    End Get
  End Property

  Private Function GetDBConnection() As String
    Dim strCurDir As String
    Dim reader As Xml.XmlTextReader
    Dim strHost As String
    Dim intPort As Integer
    Dim strSID As String
    Dim strUser As String
    Dim strPassword As String
    Dim strConnString As String
    strCurDir = Directory.GetCurrentDirectory() & "/"
    reader = New Xml.XmlTextReader(strCurDir & "Database.config")

    While reader.Read
      If reader.NodeType = Xml.XmlNodeType.Element Then
        If String.Compare("Database", reader.LocalName(), False) = 0 Then
          If reader.ReadToFollowing("Host") Then strHost = reader.ReadString
          If reader.ReadToFollowing("Port") Then intPort = reader.ReadString
          If reader.ReadToFollowing("SID") Then strSID = reader.ReadString
          If reader.ReadToFollowing("User") Then strUser = reader.ReadString
          If reader.ReadToFollowing("Password") Then strPassword = reader.ReadString
          Exit While
        End If
      End If
    End While
    reader.Close()

    strConnString = "Data Source=(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = " _
              & strHost & ")(PORT = " & intPort & "))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = " _
              & strSID & ")));Password=" & strPassword & ";User ID=" & strUser

    Return strConnString
  End Function


  Public Sub Open()
    Me.mOraConnection = New OracleConnection(Me.mConnectionString)
    Me.mOraConnection.Open()
  End Sub

  Public Sub Close()
    If Me.mOraConnection Is Nothing Then Return
    Me.mOraConnection.Close()
  End Sub

  Public Sub BeginTran()
    Me.mOraTran = Me.mOraConnection.BeginTransaction()
  End Sub

  Public Sub BeginTran(ByVal iso As IsolationLevel)
    Me.mOraTran = Me.mOraConnection.BeginTransaction()
  End Sub

  Public Sub Commit()
    Me.mOraTran.Commit()
    Me.mOraTran = CType(Nothing, OracleTransaction)
  End Sub

  Public Sub Rollback()
    If Me.mOraTran Is Nothing Then Return
    Me.mOraTran.Rollback()
    Me.mOraTran = CType(Nothing, OracleTransaction)
  End Sub

  Public Function ExecuteStored(ByVal strCommand As String, ByVal param As DbParameter) As Integer
    Me.BuildSqlCommand(strCommand, param, CommandType.StoredProcedure)
    Return Me.mOraCommand.ExecuteNonQuery()
  End Function

  Public Function ExecuteStored(ByVal strCommand As String, ByVal param As List(Of OracleParameter)) As Integer
    Me.BuildSqlCommand(strCommand, param)
    Return Me.mOraCommand.ExecuteNonQuery()
  End Function

  Public Sub FillStored(ByVal strCommand As String, ByVal param As DbParameter, ByVal dt As DataTable)
    Me.BuildSqlCommand(strCommand, param, CommandType.StoredProcedure)
    Me.mOraDataAdapter = New OracleDataAdapter()
    Me.mOraDataAdapter.SelectCommand = Me.mOraCommand
    Me.mOraDataAdapter.Fill(dt)
  End Sub

  Public Sub FillStored(ByVal strCommand As String, ByVal param As List(Of OracleParameter), ByVal dt As DataTable)
    Me.BuildSqlCommand(strCommand, param)
    Me.mOraDataAdapter = New OracleDataAdapter()
    Me.mOraDataAdapter.SelectCommand = Me.mOraCommand
    Me.mOraDataAdapter.Fill(dt)
  End Sub

  Public Function ExecuteSql(ByVal strCommand As String) As Integer
    Return Me.ExecuteSql(strCommand, 0)
  End Function

  Public Function ExecuteSql(ByVal strCommand As String, ByVal intTimeOut As Integer) As Integer
    strCommand = strCommand.Replace("''", "Chr(0)")
    Me.mOraCommand = New OracleCommand(strCommand, Me.mOraConnection)
    Me.mOraCommand.CommandType = CommandType.Text
    Me.mOraCommand.CommandTimeout = intTimeOut
    If Me.mOraTran IsNot Nothing Then Me.mOraCommand.Transaction = Me.mOraTran
    Return Me.mOraCommand.ExecuteNonQuery()
  End Function

  Public Function ExecuteSql(ByVal strCommand As String, ByVal intTimeOut As Integer, ByVal param As DbParameter) As Integer
    Me.BuildSqlCommand(strCommand, param, CommandType.Text)
    Me.mOraCommand.CommandTimeout = intTimeOut
    If Me.mOraTran IsNot Nothing Then Me.mOraCommand.Transaction = Me.mOraTran
    Return Me.mOraCommand.ExecuteNonQuery()
  End Function

  Public Function ExecuteSqlAllTran(ByVal strCommand As String) As Integer
    Return Me.ExecuteSqlAllTran(strCommand, 0, IsolationLevel.ReadCommitted)
  End Function

  Public Function ExecuteSqlAllTran(ByVal strCommand As String, ByVal iso As IsolationLevel) As Integer
    Return Me.ExecuteSqlAllTran(strCommand, 0, iso)
  End Function

  Public Function ExecuteSqlAllTran(ByVal strCommand As String, ByVal intTimeOut As Integer, ByVal Optional iso As IsolationLevel = IsolationLevel.ReadCommitted) As Integer
    strCommand = strCommand.Replace("''", "Chr(0)")
    Me.mOraCommand = New OracleCommand(strCommand, Me.mOraConnection)
    Me.mOraCommand.CommandType = CommandType.Text
    Me.mOraCommand.CommandTimeout = intTimeOut
    If Me.mOraTran IsNot Nothing Then Me.mOraCommand.Transaction = Me.mOraTran
    Return Me.mOraCommand.ExecuteNonQuery()
  End Function

  Public Function ExecuteSqlAllTran(ByVal strCommand As String, ByVal intTimeOut As Integer, ByVal param As DbParameter, ByVal Optional iso As IsolationLevel = IsolationLevel.ReadCommitted) As Integer
    Me.BuildSqlCommand(strCommand, param, CommandType.Text)
    Me.mOraCommand.CommandTimeout = intTimeOut
    If Me.mOraTran IsNot Nothing Then Me.mOraCommand.Transaction = Me.mOraTran
    Return Me.mOraCommand.ExecuteNonQuery()
  End Function

  Public Function ExecuteScalarSql(ByVal strCommand As String) As Object
    Return Me.ExecuteScalarSql(strCommand, 0)
  End Function

  Public Function ExecuteScalarSql(ByVal strCommand As String, ByVal intTimeOut As Integer) As Object
    strCommand = strCommand.Replace("''", "Chr(0)")
    Me.mOraCommand = New OracleCommand(strCommand, Me.mOraConnection)
    Me.mOraCommand.CommandType = CommandType.Text
    Me.mOraCommand.CommandTimeout = intTimeOut
    If Me.mOraTran IsNot Nothing Then Me.mOraCommand.Transaction = Me.mOraTran
    Dim obj As Object = RuntimeHelpers.GetObjectValue(Me.mOraCommand.ExecuteScalar())
    If Operators.CompareString(obj.[GetType]().FullName, "System.String", False) = 0 Then obj = CObj(Conversions.ToString(obj).Replace(vbNullChar, ""))
    Return obj
  End Function

  Public Function ExecuteScalarSql(ByVal strCommand As String, ByVal intTimeOut As Integer, ByVal param As DbParameter) As Object
    Me.BuildSqlCommand(strCommand, param, CommandType.Text)
    Me.mOraCommand.CommandTimeout = intTimeOut
    If Me.mOraTran IsNot Nothing Then Me.mOraCommand.Transaction = Me.mOraTran
    Dim obj As Object = RuntimeHelpers.GetObjectValue(Me.mOraCommand.ExecuteScalar())
    If Operators.CompareString(obj.[GetType]().FullName, "System.String", False) = 0 Then obj = CObj(Conversions.ToString(obj).Replace(vbNullChar, ""))
    Return obj
  End Function

  Public Function ExecuteScalarSqlAllTran(ByVal strCommand As String) As Object
    Return Me.ExecuteScalarSqlAllTran(strCommand, 0, IsolationLevel.ReadCommitted)
  End Function

  Public Function ExecuteScalarSqlAllTran(ByVal strCommand As String, ByVal intTimeOut As Integer, ByVal Optional iso As IsolationLevel = IsolationLevel.ReadCommitted) As Object
    strCommand = strCommand.Replace("''", "Chr(0)")
    Me.mOraCommand = New OracleCommand(strCommand, Me.mOraConnection)
    Me.mOraCommand.CommandType = CommandType.Text
    Me.mOraCommand.CommandTimeout = intTimeOut
    If Me.mOraTran IsNot Nothing Then Me.mOraCommand.Transaction = Me.mOraTran
    Dim obj As Object = RuntimeHelpers.GetObjectValue(Me.mOraCommand.ExecuteScalar())
    If Operators.CompareString(obj.[GetType]().FullName, "System.String", False) = 0 Then obj = CObj(Conversions.ToString(obj).Replace(vbNullChar, ""))
    Return obj
  End Function

  Public Function ExecuteScalarSqlAllTran(ByVal strCommand As String, ByVal intTimeOut As Integer, ByVal param As DbParameter, ByVal Optional iso As IsolationLevel = IsolationLevel.ReadCommitted) As Object
    Me.BuildSqlCommand(strCommand, param, CommandType.Text)
    Me.mOraCommand.CommandTimeout = intTimeOut
    If Me.mOraTran IsNot Nothing Then Me.mOraCommand.Transaction = Me.mOraTran
    Dim obj As Object = RuntimeHelpers.GetObjectValue(Me.mOraCommand.ExecuteScalar())
    If Operators.CompareString(obj.[GetType]().FullName, "System.String", False) = 0 Then obj = CObj(Conversions.ToString(obj).Replace(vbNullChar, ""))
    Return obj
  End Function

  Public Sub FillSql(ByVal strCommand As String, ByRef dt As DataTable)
    Me.FillSql(strCommand, 0, dt)
  End Sub

  Public Sub FillSql(ByVal strCommand As String, ByVal intTimeOut As Integer, ByRef dt As DataTable)
    strCommand = strCommand.Replace("''", "Chr(0)")
    Me.mOraCommand = New OracleCommand(strCommand, Me.mOraConnection)
    Me.mOraCommand.CommandType = CommandType.Text
    Me.mOraCommand.CommandTimeout = intTimeOut
    If Me.mOraTran IsNot Nothing Then Me.mOraCommand.Transaction = Me.mOraTran
    Me.mOraDataAdapter = New OracleDataAdapter()
    Me.mOraDataAdapter.SelectCommand = Me.mOraCommand
    Me.mOraDataAdapter.Fill(dt)
    Me.ChangeChar0toEmpty(dt)
  End Sub

  Public Sub FillSql(ByVal strCommand As String, ByVal intTimeOut As Integer, ByVal param As DbParameter, ByRef dt As DataTable)
    Me.BuildSqlCommand(strCommand, param, CommandType.Text)
    Me.mOraCommand.CommandTimeout = intTimeOut
    Me.mOraDataAdapter = New OracleDataAdapter()
    Me.mOraDataAdapter.SelectCommand = Me.mOraCommand
    Me.mOraDataAdapter.Fill(dt)
    Me.ChangeChar0toEmpty(dt)
  End Sub

  Public Sub FillSqlAllTran(ByVal strCommand As String, ByRef dt As DataTable)
    Me.FillSql(strCommand, 0, dt)
  End Sub

  Public Sub FillSqlAllTran(ByVal strCommand As String, ByVal intTimeOut As Integer, ByRef dt As DataTable, ByVal Optional iso As IsolationLevel = IsolationLevel.ReadCommitted)
    strCommand = strCommand.Replace("''", "Chr(0)")
    Me.mOraCommand = New OracleCommand(strCommand, Me.mOraConnection)
    Me.mOraCommand.CommandType = CommandType.Text
    Me.mOraCommand.CommandTimeout = intTimeOut
    If Me.mOraTran IsNot Nothing Then Me.mOraCommand.Transaction = Me.mOraTran
    Me.mOraDataAdapter = New OracleDataAdapter()
    Me.mOraDataAdapter.SelectCommand = Me.mOraCommand
    Me.mOraDataAdapter.Fill(dt)
    Me.ChangeChar0toEmpty(dt)
  End Sub

  Public Sub FillSqlAllTran(ByVal strCommand As String, ByVal intTimeOut As Integer, ByVal param As DbParameter, ByRef dt As DataTable, ByVal Optional iso As IsolationLevel = IsolationLevel.ReadCommitted)
    Me.BuildSqlCommand(strCommand, param, CommandType.Text)
    Me.mOraCommand.CommandTimeout = intTimeOut
    Me.mOraDataAdapter = New OracleDataAdapter()
    Me.mOraDataAdapter.SelectCommand = Me.mOraCommand
    Me.mOraDataAdapter.Fill(dt)
    Me.ChangeChar0toEmpty(dt)
  End Sub

  Private Sub ChangeChar0toEmpty(ByRef dt As DataTable)
    Try

      For Each row As DataRow In dt.Rows

        Try

          For Each column As DataColumn In CType(dt.Columns, InternalDataCollectionBase)
            If row(column) <> Nothing AndAlso Operators.CompareString(column.DataType.ToString(), "System.String", False) = 0 Then row(column) = CObj(Conversions.ToString(row(column)).Replace(vbNullChar, ""))
          Next

        Finally
          Dim enumerator As IEnumerator = Nothing
          If TypeOf enumerator Is IDisposable Then
            Dim tempValue = TryCast(enumerator, IDisposable)
            tempValue.Dispose()
          End If
        End Try
      Next

    Finally
      Dim enumerator As IEnumerator = Nothing
      If TypeOf enumerator Is IDisposable Then
        Dim tempValue = TryCast(enumerator, IDisposable)
        tempValue.Dispose()
      End If
    End Try

    dt.AcceptChanges()
  End Sub

  Protected Sub BuildSqlCommand(ByVal strCommand As String, ByVal param As DbParameter, ByVal CommandType As CommandType)
    strCommand = strCommand.Replace("''", "Chr(0)")
    Me.mOraCommand = New OracleCommand(strCommand, Me.mOraConnection)
    Me.mOraCommand.CommandType = CommandType
    Me.mOraCommand.CommandTimeout = 0
    If Me.mOraTran IsNot Nothing Then Me.mOraCommand.Transaction = Me.mOraTran
    Dim enumerator As List(Of OracleParameter).Enumerator = New List(Of OracleParameter).Enumerator()

    Try
      enumerator = (CType(param.Paremeters, List(Of OracleParameter))).GetEnumerator()

      While enumerator.MoveNext()
        Me.mOraCommand.Parameters.Add(enumerator.Current)
      End While

    Finally
      enumerator.Dispose()
    End Try
  End Sub

  Protected Sub BuildSqlCommand(ByVal strCommand As String, ByVal param As List(Of OracleParameter))
    strCommand = strCommand.Replace("''", "Chr(0)")
    Me.mOraCommand = New OracleCommand(strCommand, Me.mOraConnection)
    Me.mOraCommand.CommandType = CommandType.StoredProcedure
    Me.mOraCommand.CommandTimeout = 0
    If Me.mOraTran IsNot Nothing Then Me.mOraCommand.Transaction = Me.mOraTran
    Dim enumerator As List(Of OracleParameter).Enumerator = New List(Of OracleParameter).Enumerator()

    Try
      enumerator = param.GetEnumerator()

      While enumerator.MoveNext()
        Me.mOraCommand.Parameters.Add(enumerator.Current)
      End While

    Finally
      enumerator.Dispose()
    End Try
  End Sub

  Protected Overridable Sub Dispose(ByVal disposing As Boolean)
    If Not Me.disposedValue AndAlso disposing Then Me.mOraConnection.Dispose()
    Me.disposedValue = True
  End Sub

  Public Sub Dispose()
    Me.Dispose(True)
    GC.SuppressFinalize(CObj(Me))
  End Sub

  Public Overrides Function ToString() As String
    Return Me.mOraCommand.CommandText
  End Function

  Public Shared Function IsExclusionErrorForOracle(ByVal sqlex As OracleException) As Boolean
    Return False
  End Function

  Public Sub OutputLog(ByVal addvalue As String)
    Try
      Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
      Dim streamWriter As StreamWriter = New StreamWriter("C:\sql.log", True, Encoding.GetEncoding("sjis"))
      streamWriter.WriteLine(DateAndTime.Now.ToString() & vbTab & addvalue)
      streamWriter.Close()
    Catch ex As Exception
      ProjectData.SetProjectError(ex)
      ProjectData.ClearProjectError()
    End Try
  End Sub

  Private Sub IDisposable_Dispose() Implements IDisposable.Dispose
    Throw New NotImplementedException()
  End Sub

End Class
