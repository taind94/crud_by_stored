﻿Imports System.Security.Cryptography
Imports System.Text

Public Class CommonHelper
  Public Shared Function Hash(ByVal strSalt As String, ByVal strPassword As String) As String
    Try
      'Convert saft string to byte
      Dim saltByte = Encoding.UTF8.GetBytes(strSalt)

      'Convert data string to byte
      Dim codeByte = Encoding.UTF8.GetBytes(strPassword)

      'Hash password with MD5
      Dim hmacMD5 = New HMACMD5(saltByte)
      Dim saltedHash = hmacMD5.ComputeHash(codeByte)

      'Create a New Stringbuilder to collect the bytes and create a string.
      Dim sBuilder As StringBuilder = New StringBuilder()

      'Loop through each byte of the hashed data and format each one as a hexadecimal string.
      For i As Integer = 0 To saltedHash.Length - 1
        sBuilder.Append(saltedHash(i).ToString("x2"))
      Next

      Return sBuilder.ToString()
    Catch e As Exception
      Console.WriteLine(e)
      Throw
    End Try
  End Function
End Class
