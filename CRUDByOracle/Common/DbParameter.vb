﻿Imports System.Data.OracleClient
Imports System.Runtime.CompilerServices

Public Class DbParameter
  Private mOraParemeters As List(Of OracleParameter)

  Public Sub New()
    Me.mOraParemeters = New List(Of OracleParameter)()
  End Sub

  Public ReadOnly Property Paremeters As Object
    Get
      Return CObj(Me.mOraParemeters)
    End Get
  End Property

  Public Sub AddParemeter(ByVal parameterName As String, ByVal dbType As DbParameter.DbType, ByVal size As Integer, ByVal value As Object)
    Me.AddParemeter(parameterName, dbType, size, RuntimeHelpers.GetObjectValue(value), ParameterDirection.Input)
  End Sub

  Public Sub AddParemeter(ByVal parameterName As String, ByVal dbType As DbParameter.DbType, ByVal size As Integer, ByVal value As Object, ByVal Direction As ParameterDirection)
    Dim strName As String = parameterName
    Select Case dbType
      Case DbParameter.DbType.INTEGER_
        Me.mOraParemeters.Add(New OracleParameter(strName, OracleType.Int32))
      Case DbParameter.DbType.SMALLINT
        Me.mOraParemeters.Add(New OracleParameter(strName, OracleType.Int16))
      Case DbParameter.DbType.CHAR_
        Me.mOraParemeters.Add(New OracleParameter(strName, OracleType.Char, size))
      Case DbParameter.DbType.VARCHAR
        Me.mOraParemeters.Add(New OracleParameter(strName, OracleType.VarChar, size))
      Case DbParameter.DbType.NVARCHAR
        Me.mOraParemeters.Add(New OracleParameter(strName, OracleType.NVarChar, size))
      Case DbParameter.DbType.NCHAR
        Me.mOraParemeters.Add(New OracleParameter(strName, OracleType.NChar, size))
      Case DbParameter.DbType.MONEY
        Me.mOraParemeters.Add(New OracleParameter(strName, OracleType.Number, size))
      Case DbParameter.DbType.Binary
        Me.mOraParemeters.Add(New OracleParameter(strName, OracleType.Blob))
      Case DbParameter.DbType.Float
        Me.mOraParemeters.Add(New OracleParameter(strName, OracleType.Float, size))
      Case DbParameter.DbType.Decimal_
        Me.mOraParemeters.Add(New OracleParameter(strName, OracleType.Number, size))
      Case DbParameter.DbType.BIGINT
        Me.mOraParemeters.Add(New OracleParameter(strName, OracleType.Number, 19))
      Case DbParameter.DbType.Date_
        Me.mOraParemeters.Add(New OracleParameter(strName, OracleType.DateTime))
      Case DbParameter.DbType.DateTime
        Me.mOraParemeters.Add(New OracleParameter(strName, OracleType.DateTime))
      Case DbParameter.DbType.Cursor
        Dim param As OracleParameter = New OracleParameter(strName, OracleType.Cursor)
        Me.mOraParemeters.Add(New OracleParameter(strName, OracleType.Cursor))
      Case Else
        Return
    End Select

    Me.mOraParemeters(Me.mOraParemeters.Count - 1).Direction = Direction

    If value IsNot Nothing AndAlso CompilerServices.Operators.CompareString(value.ToString(), String.Empty, False) = 0 Then
      Me.mOraParemeters(Me.mOraParemeters.Count - 1).Value = String.Empty

    Else
      Me.mOraParemeters(Me.mOraParemeters.Count - 1).Value = RuntimeHelpers.GetObjectValue(value)
    End If
  End Sub

  Public Enum DbType
    INTEGER_ = 1
    SMALLINT = 2
    CHAR_ = 3
    VARCHAR = 4
    NVARCHAR = 5
    NCHAR = 6
    MONEY = 7
    Binary = 8
    Float = 9
    Decimal_ = 10
    BIGINT = 11
    Date_ = 12
    DateTime = 13
    Cursor = 14
  End Enum
End Class
