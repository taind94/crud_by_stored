﻿Public Class ProductForm

  Private cn As DbConnection = New DbConnection()
  Private dt As DataTable = New DataTable()

  Private Sub ProductForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    LoadGridview()
  End Sub

  Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

    Dim strQuery As String
    Dim param As DbParameter = New DbParameter()

    If Trim(txtxName.Text) = vbNullString Then
      Return
    End If

    param.AddParemeter(":name", DbParameter.DbType.NVARCHAR, 100, Trim(txtxName.Text))
    strQuery = "INSERT INTO PRODUCTS(NAME) " _
             & "VALUES(:name)"
    cn.Open()
    cn.ExecuteSql(strQuery, 0, param)
    cn.Close()

    LoadGridview()
  End Sub

  Private Sub LoadGridview()

    Dim strQuery As String
    strQuery = "SELECT * " _
             & "FROM PRODUCTS " _
             & "ORDER BY ID"
    dt = New DataTable()
    cn.Open()
    cn.FillSql(strQuery, dt)
    cn.Close()
    dgvUser.DataSource = dt

    'Clear content
    dgvUser.ClearSelection()
    txtxName.Text = String.Empty
    txtID.Text = String.Empty
  End Sub

  Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
    Dim strQuery As String
    Dim param As DbParameter = New DbParameter()

    If Trim(txtxName.Text) = vbNullString OrElse Trim(txtID.Text) = vbNullString Then
      Return
    End If

    param.AddParemeter(":name", DbParameter.DbType.NVARCHAR, 100, Trim(txtxName.Text))
    param.AddParemeter(":id", DbParameter.DbType.INTEGER_, 4, CInt(Trim(txtID.Text)))
    strQuery = "UPDATE PRODUCTS " _
             & "SET NAME =:name " _
             & "WHERE ID =:id"
    cn.Open()
    cn.ExecuteSql(strQuery, 0, param)
    cn.Close()

    LoadGridview()
  End Sub

  Private Sub dgvUser_SelectionChanged(sender As Object, e As EventArgs) Handles dgvUser.SelectionChanged
    txtID.Text = IIf(dgvUser.CurrentRow.Cells(0).Value.ToString <> vbNullString, dgvUser.CurrentRow.Cells(0).Value, String.Empty)
    txtxName.Text = IIf(dgvUser.CurrentRow.Cells(1).Value.ToString <> vbNullString, dgvUser.CurrentRow.Cells(1).Value, String.Empty)
  End Sub

  Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
    Dim strQuery As String
    Dim param As DbParameter = New DbParameter()

    If Trim(txtxName.Text) = vbNullString OrElse Trim(txtID.Text) = vbNullString Then
      Return
    End If

    param.AddParemeter(":id", DbParameter.DbType.INTEGER_, 4, CInt(Trim(txtID.Text)))
    strQuery = "DELETE FROM PRODUCTS " _
             & "WHERE ID =:id"
    cn.Open()
    cn.ExecuteSql(strQuery, 0, param)
    cn.Close()

    LoadGridview()
  End Sub
End Class
