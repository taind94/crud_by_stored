﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserEditForm
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.cbbStatus = New System.Windows.Forms.ComboBox()
    Me.cbbDepartment = New System.Windows.Forms.ComboBox()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.lblUsercode = New System.Windows.Forms.Label()
    Me.txtEmail = New System.Windows.Forms.TextBox()
    Me.txtUserName = New System.Windows.Forms.TextBox()
    Me.btnBack = New System.Windows.Forms.Button()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.btnEdit = New System.Windows.Forms.Button()
    Me.btnClear = New System.Windows.Forms.Button()
    Me.txtUserID = New System.Windows.Forms.TextBox()
    Me.Label4 = New System.Windows.Forms.Label()
    Me.Panel3 = New System.Windows.Forms.Panel()
    Me.Label5 = New System.Windows.Forms.Label()
    Me.Panel3.SuspendLayout()
    Me.SuspendLayout()
    '
    'cbbStatus
    '
    Me.cbbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cbbStatus.FormattingEnabled = True
    Me.cbbStatus.Location = New System.Drawing.Point(145, 206)
    Me.cbbStatus.Name = "cbbStatus"
    Me.cbbStatus.Size = New System.Drawing.Size(169, 21)
    Me.cbbStatus.TabIndex = 38
    '
    'cbbDepartment
    '
    Me.cbbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cbbDepartment.FormattingEnabled = True
    Me.cbbDepartment.Location = New System.Drawing.Point(145, 167)
    Me.cbbDepartment.Name = "cbbDepartment"
    Me.cbbDepartment.Size = New System.Drawing.Size(169, 21)
    Me.cbbDepartment.TabIndex = 37
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(47, 140)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(32, 13)
    Me.Label3.TabIndex = 34
    Me.Label3.Text = "Email"
    '
    'lblUsercode
    '
    Me.lblUsercode.AutoSize = True
    Me.lblUsercode.Location = New System.Drawing.Point(47, 66)
    Me.lblUsercode.Name = "lblUsercode"
    Me.lblUsercode.Size = New System.Drawing.Size(18, 13)
    Me.lblUsercode.TabIndex = 33
    Me.lblUsercode.Text = "ID"
    '
    'txtEmail
    '
    Me.txtEmail.Location = New System.Drawing.Point(145, 133)
    Me.txtEmail.Name = "txtEmail"
    Me.txtEmail.Size = New System.Drawing.Size(169, 20)
    Me.txtEmail.TabIndex = 32
    '
    'txtUserName
    '
    Me.txtUserName.Location = New System.Drawing.Point(145, 98)
    Me.txtUserName.Name = "txtUserName"
    Me.txtUserName.Size = New System.Drawing.Size(169, 20)
    Me.txtUserName.TabIndex = 31
    '
    'btnBack
    '
    Me.btnBack.Location = New System.Drawing.Point(29, 368)
    Me.btnBack.Name = "btnBack"
    Me.btnBack.Size = New System.Drawing.Size(75, 23)
    Me.btnBack.TabIndex = 30
    Me.btnBack.Text = "Back"
    Me.btnBack.UseVisualStyleBackColor = True
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(47, 214)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(37, 13)
    Me.Label2.TabIndex = 29
    Me.Label2.Text = "Status"
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(47, 175)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(62, 13)
    Me.Label1.TabIndex = 28
    Me.Label1.Text = "Department"
    '
    'btnEdit
    '
    Me.btnEdit.Location = New System.Drawing.Point(361, 368)
    Me.btnEdit.Name = "btnEdit"
    Me.btnEdit.Size = New System.Drawing.Size(75, 23)
    Me.btnEdit.TabIndex = 27
    Me.btnEdit.Text = "Edit"
    Me.btnEdit.UseVisualStyleBackColor = True
    '
    'btnClear
    '
    Me.btnClear.Location = New System.Drawing.Point(258, 368)
    Me.btnClear.Name = "btnClear"
    Me.btnClear.Size = New System.Drawing.Size(75, 23)
    Me.btnClear.TabIndex = 26
    Me.btnClear.Text = "Clear"
    Me.btnClear.UseVisualStyleBackColor = True
    '
    'txtUserID
    '
    Me.txtUserID.Location = New System.Drawing.Point(145, 63)
    Me.txtUserID.Name = "txtUserID"
    Me.txtUserID.ReadOnly = True
    Me.txtUserID.Size = New System.Drawing.Size(169, 20)
    Me.txtUserID.TabIndex = 41
    '
    'Label4
    '
    Me.Label4.AutoSize = True
    Me.Label4.Location = New System.Drawing.Point(47, 105)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(55, 13)
    Me.Label4.TabIndex = 42
    Me.Label4.Text = "Username"
    '
    'Panel3
    '
    Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
    Me.Panel3.Controls.Add(Me.Label5)
    Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
    Me.Panel3.Location = New System.Drawing.Point(0, 0)
    Me.Panel3.Name = "Panel3"
    Me.Panel3.Size = New System.Drawing.Size(458, 44)
    Me.Panel3.TabIndex = 43
    '
    'Label5
    '
    Me.Label5.AutoSize = True
    Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label5.Location = New System.Drawing.Point(12, 15)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(64, 15)
    Me.Label5.TabIndex = 0
    Me.Label5.Text = "Edit user"
    '
    'UserEditForm
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(458, 410)
    Me.Controls.Add(Me.Panel3)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.txtUserID)
    Me.Controls.Add(Me.cbbStatus)
    Me.Controls.Add(Me.cbbDepartment)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.lblUsercode)
    Me.Controls.Add(Me.txtEmail)
    Me.Controls.Add(Me.txtUserName)
    Me.Controls.Add(Me.btnBack)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.btnEdit)
    Me.Controls.Add(Me.btnClear)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
    Me.Name = "UserEditForm"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "UserEditForm"
    Me.Panel3.ResumeLayout(False)
    Me.Panel3.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

  Friend WithEvents cbbStatus As ComboBox
  Friend WithEvents cbbDepartment As ComboBox
  Friend WithEvents Label3 As Label
  Friend WithEvents lblUsercode As Label
  Friend WithEvents txtEmail As TextBox
  Friend WithEvents txtUserName As TextBox
  Friend WithEvents btnBack As Button
  Friend WithEvents Label2 As Label
  Friend WithEvents Label1 As Label
  Friend WithEvents btnEdit As Button
  Friend WithEvents btnClear As Button
  Friend WithEvents txtUserID As TextBox
  Friend WithEvents Label4 As Label
  Friend WithEvents Panel3 As Panel
  Friend WithEvents Label5 As Label
End Class
