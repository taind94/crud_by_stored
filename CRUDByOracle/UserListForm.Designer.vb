﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class UserListForm
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()>
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()>
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.dgvUser = New System.Windows.Forms.DataGridView()
    Me.btnAdd = New System.Windows.Forms.Button()
    Me.btnEdit = New System.Windows.Forms.Button()
    Me.btnRemove = New System.Windows.Forms.Button()
    Me.btnSearch = New System.Windows.Forms.Button()
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.txtSearch = New System.Windows.Forms.TextBox()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.btnExport = New System.Windows.Forms.Button()
    Me.btnClose = New System.Windows.Forms.Button()
    Me.Panel2 = New System.Windows.Forms.Panel()
    Me.Panel3 = New System.Windows.Forms.Panel()
    Me.lblDateTime = New System.Windows.Forms.Label()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.TimerDate = New System.Windows.Forms.Timer(Me.components)
    CType(Me.dgvUser, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel1.SuspendLayout()
    Me.Panel2.SuspendLayout()
    Me.Panel3.SuspendLayout()
    Me.SuspendLayout()
    '
    'dgvUser
    '
    Me.dgvUser.AllowUserToAddRows = False
    Me.dgvUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.dgvUser.Location = New System.Drawing.Point(11, 126)
    Me.dgvUser.MultiSelect = False
    Me.dgvUser.Name = "dgvUser"
    Me.dgvUser.Size = New System.Drawing.Size(763, 308)
    Me.dgvUser.TabIndex = 0
    '
    'btnAdd
    '
    Me.btnAdd.Location = New System.Drawing.Point(14, 16)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(75, 23)
    Me.btnAdd.TabIndex = 3
    Me.btnAdd.Text = "F1: Add new"
    Me.btnAdd.UseVisualStyleBackColor = True
    '
    'btnEdit
    '
    Me.btnEdit.Location = New System.Drawing.Point(160, 16)
    Me.btnEdit.Name = "btnEdit"
    Me.btnEdit.Size = New System.Drawing.Size(75, 23)
    Me.btnEdit.TabIndex = 4
    Me.btnEdit.Text = "F2: Edit"
    Me.btnEdit.UseVisualStyleBackColor = True
    '
    'btnRemove
    '
    Me.btnRemove.Location = New System.Drawing.Point(325, 16)
    Me.btnRemove.Name = "btnRemove"
    Me.btnRemove.Size = New System.Drawing.Size(75, 23)
    Me.btnRemove.TabIndex = 5
    Me.btnRemove.Text = "F3: Delete"
    Me.btnRemove.UseVisualStyleBackColor = True
    '
    'btnSearch
    '
    Me.btnSearch.Location = New System.Drawing.Point(263, 14)
    Me.btnSearch.Name = "btnSearch"
    Me.btnSearch.Size = New System.Drawing.Size(75, 23)
    Me.btnSearch.TabIndex = 8
    Me.btnSearch.Text = "Search"
    Me.btnSearch.UseVisualStyleBackColor = True
    '
    'Panel1
    '
    Me.Panel1.BackColor = System.Drawing.SystemColors.Control
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.txtSearch)
    Me.Panel1.Controls.Add(Me.btnSearch)
    Me.Panel1.Location = New System.Drawing.Point(12, 60)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(762, 51)
    Me.Panel1.TabIndex = 9
    '
    'txtSearch
    '
    Me.txtSearch.Location = New System.Drawing.Point(5, 15)
    Me.txtSearch.Name = "txtSearch"
    Me.txtSearch.Size = New System.Drawing.Size(254, 20)
    Me.txtSearch.TabIndex = 0
    Me.txtSearch.Text = "Search by user name or email...."
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(23, 54)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(95, 13)
    Me.Label2.TabIndex = 10
    Me.Label2.Text = "Search information"
    '
    'btnExport
    '
    Me.btnExport.Location = New System.Drawing.Point(494, 16)
    Me.btnExport.Name = "btnExport"
    Me.btnExport.Size = New System.Drawing.Size(75, 23)
    Me.btnExport.TabIndex = 11
    Me.btnExport.Text = "F4: Export"
    Me.btnExport.UseVisualStyleBackColor = True
    '
    'btnClose
    '
    Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnClose.Location = New System.Drawing.Point(663, 16)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 23)
    Me.btnClose.TabIndex = 12
    Me.btnClose.Text = "F5: Close"
    Me.btnClose.UseVisualStyleBackColor = True
    '
    'Panel2
    '
    Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel2.Controls.Add(Me.btnAdd)
    Me.Panel2.Controls.Add(Me.btnClose)
    Me.Panel2.Controls.Add(Me.btnEdit)
    Me.Panel2.Controls.Add(Me.btnExport)
    Me.Panel2.Controls.Add(Me.btnRemove)
    Me.Panel2.Location = New System.Drawing.Point(12, 450)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(763, 59)
    Me.Panel2.TabIndex = 13
    '
    'Panel3
    '
    Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
    Me.Panel3.Controls.Add(Me.lblDateTime)
    Me.Panel3.Controls.Add(Me.Label1)
    Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
    Me.Panel3.Location = New System.Drawing.Point(0, 0)
    Me.Panel3.Name = "Panel3"
    Me.Panel3.Size = New System.Drawing.Size(796, 44)
    Me.Panel3.TabIndex = 14
    '
    'lblDateTime
    '
    Me.lblDateTime.AutoSize = True
    Me.lblDateTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblDateTime.Location = New System.Drawing.Point(641, 15)
    Me.lblDateTime.Name = "lblDateTime"
    Me.lblDateTime.Size = New System.Drawing.Size(79, 15)
    Me.lblDateTime.TabIndex = 1
    Me.lblDateTime.Text = "yyyy/MM/dd"
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label1.Location = New System.Drawing.Point(12, 15)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(60, 15)
    Me.Label1.TabIndex = 0
    Me.Label1.Text = "User list"
    '
    'TimerDate
    '
    '
    'UserListForm
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(796, 523)
    Me.Controls.Add(Me.Panel3)
    Me.Controls.Add(Me.Panel2)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.dgvUser)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
    Me.Name = "UserListForm"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "User list"
    CType(Me.dgvUser, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    Me.Panel2.ResumeLayout(False)
    Me.Panel3.ResumeLayout(False)
    Me.Panel3.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

  Friend WithEvents dgvUser As DataGridView
  Friend WithEvents btnAdd As Button
  Friend WithEvents btnEdit As Button
  Friend WithEvents btnRemove As Button
  Friend WithEvents btnSearch As Button
  Friend WithEvents Panel1 As Panel
  Friend WithEvents txtSearch As TextBox
  Friend WithEvents Label2 As Label
  Friend WithEvents btnExport As Button
  Friend WithEvents btnClose As Button
  Friend WithEvents Panel2 As Panel
  Friend WithEvents Panel3 As Panel
  Friend WithEvents Label1 As Label
  Friend WithEvents lblDateTime As Label
  Friend WithEvents TimerDate As Timer
End Class
